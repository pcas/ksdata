module bitbucket.org/pcas/ksdata

go 1.13

require (
	bitbucket.org/pcas/keyvalue v0.2.0
	bitbucket.org/pcas/logger v0.1.35
	bitbucket.org/pcas/metrics v0.1.27
	bitbucket.org/pcas/sslflag v0.0.14
	bitbucket.org/pcastools/address v0.1.4 // indirect
	bitbucket.org/pcastools/bytesbuffer v1.0.3 // indirect
	bitbucket.org/pcastools/cleanup v1.0.4
	bitbucket.org/pcastools/contextutil v1.0.3 // indirect
	bitbucket.org/pcastools/convert v1.0.5
	bitbucket.org/pcastools/fatal v1.0.3 // indirect
	bitbucket.org/pcastools/file v1.0.3
	bitbucket.org/pcastools/flag v0.0.18
	bitbucket.org/pcastools/gobutil v1.0.4 // indirect
	bitbucket.org/pcastools/grpcutil v1.0.13 // indirect
	bitbucket.org/pcastools/listenutil v0.0.10 // indirect
	bitbucket.org/pcastools/log v1.0.4
	bitbucket.org/pcastools/pool v1.0.4 // indirect
	bitbucket.org/pcastools/rand v1.0.4 // indirect
	bitbucket.org/pcastools/slice v1.0.4
	bitbucket.org/pcastools/stringsbuilder v1.0.3
	bitbucket.org/pcastools/ulid v0.1.6 // indirect
	bitbucket.org/pcastools/version v0.0.5
	github.com/Shopify/toxiproxy v2.1.4+incompatible // indirect
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/aws/aws-sdk-go v1.36.15 // indirect
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/gobuffalo/genny v0.1.1 // indirect
	github.com/gobuffalo/gogen v0.1.1 // indirect
	github.com/karrick/godirwalk v1.10.3 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pelletier/go-toml v1.7.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	golang.org/x/exp v0.0.0-20200331195152-e8c3332aa8e5 // indirect
	gopkg.in/jcmturner/aescts.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/dnsutils.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/goidentity.v3 v3.0.0 // indirect
	gopkg.in/jcmturner/gokrb5.v7 v7.5.0 // indirect
	gopkg.in/jcmturner/rpc.v1 v1.1.0 // indirect
)
