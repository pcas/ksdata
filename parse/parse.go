// Parse defines functions for reading descriptions of 4D reflexive polytopes from gzipped CSV files.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package parse

import (
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/ksdata/convert"
	"bufio"
	"compress/gzip"
	"encoding/csv"
	"fmt"
	"io"
	"os"
)

// Error describes the context surrounding a parse error.
type Error struct {
	File string // The file (if applicable)
	Line int    // The line number (if applicable)
	Err  error  // The error
}

/////////////////////////////////////////////////////////////////////////
// Error functions
/////////////////////////////////////////////////////////////////////////

// Cause returns the underlying error.
func (e *Error) Cause() error {
	if e == nil {
		return nil
	}
	return e.Err
}

// Error returns an error message.
func (e *Error) Error() string {
	if e == nil {
		return ""
	} else if len(e.File) == 0 {
		if e.Line == 0 {
			return fmt.Sprintf("%s", e.Err)
		}
		return fmt.Sprintf("%d: %s", e.Line, e.Err)
	} else if e.Line == 0 {
		return fmt.Sprintf("%s: %s", e.File, e.Err)
	}
	return fmt.Sprintf("%s:%d: %s", e.File, e.Line, e.Err)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// readCSV wraps the given io.Reader as a csv.Reader and attempts to read the contents. The first line of CSV data is assumed to describe the keys, with subsequent lines the corresponding values. The results will be passed down the channel resC.  Closing the doneC channel will stop the read.
func readCSV(r io.Reader, path string, resC chan<- record.Record, doneC <-chan struct{}) error {
	// Wrap the reader in a CSV reader
	csvr := csv.NewReader(bufio.NewReader(r))
	// The first line is the list of keys
	line := 1
	keys, err := csvr.Read()
	if err == io.EOF {
		return nil
	} else if err != nil {
		return &Error{
			File: path,
			Line: line,
			Err:  err,
		}
	}
	// Work through the file
	for {
		line++
		// Read in the next line of data
		if values, err := csvr.Read(); err != nil {
			// If this is EOF then we're done
			if err == io.EOF {
				return nil
			}
			// Otherwise we return an error
			return &Error{
				File: path,
				Line: line,
				Err:  err,
			}
		} else if r, err := convert.FromKeyValueSlices(keys, values); err != nil {
			return &Error{
				File: path,
				Line: line,
				Err:  err,
			}
		} else {
			// Try to pass the record down the channel
			select {
			case <-doneC:
				return nil // We've been asked to exit
			case resC <- r:
			}
		}
	}
}

// readGzipedCSVFile parses the file indicated by the given path. The file is assumed to be compressed in gzip format, with the content in CSV format. The first line of CSV data is assumed to describe the keys, with subsequent lines the corresponding values. The results will be passed down the channel resC. Closing the doneC channel will stop the read.
func readGzipedCSVFile(path string, resC chan<- record.Record, doneC <-chan struct{}) error {
	// Open the file
	fh, err := os.Open(path)
	if err != nil {
		return &Error{
			File: path,
			Err:  err,
		}
	}
	defer fh.Close()
	// Wrap the reader in a gzip decompressor
	dec, err := gzip.NewReader(bufio.NewReader(fh))
	if err != nil {
		return &Error{
			File: path,
			Err:  err,
		}
	}
	defer dec.Close()
	// Start parsing the content
	return readCSV(dec, path, resC, doneC)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// IteratorFromGzipedCSVFile returns an iterator of record.Records read from the GZIP-compressed CSV file path.
func IteratorFromGzipedCSVFile(path string) record.Iterator {
	// Create the communication channels
	resC := make(chan record.Record, 10)
	errC := make(chan error)
	doneC := make(chan struct{})
	// Start the background worker running
	go func() {
		err := readGzipedCSVFile(path, resC, doneC)
		// Iteration has finished, so tidy up
		close(resC)
		if err != nil {
			errC <- err
		}
		close(errC)
	}()
	// Create the iterator and return
	return record.NewIterator(resC, errC, doneC)
}
