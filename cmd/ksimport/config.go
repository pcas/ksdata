// Config.go handles configuration and logging for ksimport.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/keyvalue/kvdb/kvdbflag"
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/metrics/metricsdb/metricsdbflag"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
	"context"
	"fmt"
	"os"
)

// Options describes the options.
type Options struct {
	DirName      string // The directory where the .csv.gz files are
	DatabaseName string // The name of the kvdb database to write to
	TableName    string // The name of the kvdb table to write to
}

// Name is the name of the executable.
const Name = "ksimport"

// The default values
const (
	DefaultDatabaseName = "ks"
	DefaultTableName    = "reflexive4"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	return &Options{
		DatabaseName: DefaultDatabaseName,
		TableName:    DefaultTableName,
	}
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	// Create the standard SSL flag
	sslFlag := &sslflag.Flag{}
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s imports the Kreuzer-Skarke database to a pcas kvdb database.\n\nUsage: %s [options]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.String("database", &opts.DatabaseName, opts.DatabaseName, "The name of the database to write to", ""),
		flag.String("dir", &opts.DirName, opts.DirName, "The directory containing the gzipped CSV files", ""),
		flag.String("table", &opts.TableName, opts.TableName, "The name of the table to write to", ""),
		&version.Flag{AppName: Name},
		sslFlag,
	)
	// Create and add the kvdb flag set
	kvdbSet := kvdbflag.NewSet(nil)
	flag.AddSet(kvdbSet)
	// Create and add the logd flag set
	logdSet := logdflag.NewLogSet(nil)
	flag.AddSet(logdSet)
	// Create and add the metricsdb flag set
	metricsdbSet := metricsdbflag.NewMetricsSet(nil)
	flag.AddSet(metricsdbSet)
	// Parse the flags
	flag.Parse()
	// Recover the SSL certificate
	cert, err := sslFlag.Certificate()
	if err != nil {
		return err
	}
	// Set the metrics
	if err := metricsdbSet.SetMetrics(context.Background(), Name, cert); err != nil {
		return err
	}
	// Set the kvdb values
	kvdbSet.SetDefault(Name, cert)
	// Set the loggers
	return logdSet.SetLogger(context.Background(), Name, cert)

}
