// Ksimport imports the Kreuzer--Skarke database of reflexive 4-topes into a pcas kvdbd database.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/kvdb"
	_ "bitbucket.org/pcas/keyvalue/kvdb/kvdbdriver"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/ksdata/parse"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/runtimemetrics"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/file"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"
)

// inserter is the interface satisfied by the Insert method.
type inserter interface {
	Insert(context.Context, record.Iterator) error
}

// walkFilterFunc is a function that can be used to filter paths for the walk.
type walkFilterFunc func(string) bool

// InsertTimeout is the timeout for inserting records from one CSV file.
var InsertTimeout = 6 * time.Hour

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// logWalkErrors logs the errors from srcC to lg. Indicates when finished via wg. Intended to be called in its own go-routine.
func logWalkErrors(srcC <-chan error, lg log.Interface, wg *sync.WaitGroup) {
	for err := range srcC {
		if e, ok := err.(*file.WalkError); ok {
			lg.Printf("Error whilst walking files, at file %v: %v", e.File, e.Err)
		} else {
			lg.Printf("Error whilst walking files: %v", err)
		}
	}
	wg.Done()
}

// execWalkFilterFunc executes the filter function, recovering from any panics.
func execWalkFilterFunc(f walkFilterFunc, path string) (ok bool, err error) {
	// Recover from any panics caused by the user's filter function f
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("Panic in walkFilterFunc: %s", e)
		}
	}()
	// Execute the filter function
	ok = f(path)
	return
}

// walkGzipedCSVFiles walks the directory structure from the given root, looking for files whose paths satisfy the given filter function (a nil value for filter will default to only parsing file with suffix ".csv.gz"). The filenames will be passed down the returned channel. Any errors will be logged to lg. The returned channel will be closed when parsing stops.
func walkGzipedCSVFiles(root string, filter walkFilterFunc, lg log.Interface) <-chan string {
	// Sanity check
	if filter == nil {
		filter = func(path string) bool {
			return strings.HasSuffix(path, ".csv.gz")
		}
	}
	// Create the communication channels
	resC := make(chan string, 10)
	// Start the background go-routine to walk the files
	go func(resC chan<- string) {
		// Defer closing the channel
		defer close(resC)
		// Start walking the files based at the root
		pathC, walkErrC, doneC := file.Walk(root)
		defer close(doneC)
		// Log any walk errors
		wg := &sync.WaitGroup{}
		wg.Add(1)
		go logWalkErrors(walkErrC, lg, wg)
		// Start parsing the files
		wg.Add(1)
		go func() {
			defer wg.Done()
			for path := range pathC {
				// Does this path satisfy the filter?
				if ok, err := execWalkFilterFunc(filter, path); err != nil {
					lg.Printf("Error whilst filtering path %v: %v", path, err)
				} else if ok {
					// Start parsing the content
					resC <- path
				}
			}
		}()
		// Wait for the wait group to become done
		wg.Wait()
	}(resC)
	// Return the channel
	return resC
}

// importFiles will insert the the *.csv.gz files based at root into t. Returns the first error on insert, if any.
func importFiles(ctx context.Context, root string, t inserter, lg log.Interface) error {
	// Grab the filenames
	pathC := walkGzipedCSVFiles(root, nil, lg)
	// Launch the workers
	var wg sync.WaitGroup
	numWorkers := runtime.NumCPU()
	wg.Add(numWorkers)
	errC := make(chan error, numWorkers)
	for n := 0; n < numWorkers; n++ {
		go func(n int) {
			defer wg.Done()
			workerLg := log.PrefixWith(lg, "[worker %d]", n)
			workerLg.Printf("Starting")
			for path := range pathC {
				workerLg.Printf("Processing %v", path)
				// insert the records from this file
				itr := parse.IteratorFromGzipedCSVFile(path)
				workerCtx, cancel := context.WithTimeout(ctx, InsertTimeout)
				err := t.Insert(workerCtx, itr)
				workerLg.Printf("Finished iterating.  err = %v", err)
				cancel()
				// check for errors
				if err == nil {
					if err = itr.Close(); err == nil {
						err = itr.Err()
					}
				}
				workerLg.Printf("Finished closing.  err = %v", err)
				if err != nil {
					workerLg.Printf("Error when inserting records from %v: %v", path, err)
					errC <- err
					return
				}
			}
			workerLg.Printf("Exiting")
		}(n)
	}
	// Wait for the workers to finish, then return the first error (if any)
	wg.Wait()
	close(errC)
	return <-errC
}

// importData imports the Kreuzer--Skarke data from the CSV files to the database.
func importData(ctx context.Context, opts *Options, lg log.Interface) error {
	// Connect to the database
	dataSource := kvdb.DefaultConfig().URL(opts.DatabaseName)
	connCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	c, err := keyvalue.Open(connCtx, dataSource)
	cancel()
	if err != nil {
		return err
	}
	defer c.Close()
	// Connect to the table
	connCtx, cancel = context.WithTimeout(ctx, 5*time.Second)
	t, err := c.ConnectToTable(connCtx, opts.TableName)
	cancel()
	if err != nil {
		return err
	}
	defer t.Close()
	// Import the files
	return importFiles(ctx, opts.DirName, t, lg)
}

/////////////////////////////////////////////////////////////////////////
// Main function
/////////////////////////////////////////////////////////////////////////

// runMain executes the main function, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse the options and make a note of the logger
	opts := setOptions()
	lg := log.Log()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Record CPU usage etc
	defer func() {
		if e := runtimemetrics.Start(metrics.Metrics()).Stop(); err == nil {
			err = e
		}
	}()
	// Recover and log any panics
	defer func() {
		if e := recover(); e != nil {
			lg.Printf("Panic in main: %v", e)
			if err == nil {
				err = fmt.Errorf("panic in main: %v", e)
			}
		}
	}()
	// Import the data
	err = importData(ctx, opts, lg)
	return
}

func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
