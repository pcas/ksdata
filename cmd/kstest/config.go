// Config.go handles configuration and logging for kstest.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/keyvalue/kvdb/kvdbflag"
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/metrics/metricsdb/metricsdbflag"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
	"context"
	"errors"
	"fmt"
	"os"
)

// Options describes the options.
type Options struct {
	NumVertices    int    // The number of vertices that we insist on
	DatabaseName   string // The name of the database to write to
	TableName      string // The name of the table to write to
	KSDatabaseName string // The name of the database to read from
	KSTableName    string // The name of the table to read from
}

// Name is the name of the executable.
const Name = "kstest"

// The default values
const (
	DefaultDatabaseName   = "test"
	DefaultTableName      = "test"
	DefaultKSDatabaseName = "ks"
	DefaultKSTableName    = "reflexive4"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse and validate the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	return &Options{
		DatabaseName:   DefaultDatabaseName,
		TableName:      DefaultTableName,
		KSDatabaseName: DefaultKSDatabaseName,
		KSTableName:    DefaultKSTableName,
	}
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	// Create the standard SSL flag
	sslFlag := &sslflag.Flag{}
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s tests the Kreuzer-Skarke reflexive4 database, by building a table containing only the polytopes with the specified number of vertices.\n\nUsage: %s [flags]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.Int("n", &opts.NumVertices, opts.NumVertices, "The number of vertices", ""),
		flag.String("dst-database", &opts.DatabaseName, opts.DatabaseName, "The name of the database to write to", ""),
		flag.String("dst-table", &opts.TableName, opts.TableName, "The name of the table to write to", ""),
		flag.String("src-database", &opts.KSDatabaseName, opts.KSDatabaseName, "The name of the database to read from", ""),
		flag.String("src-table", &opts.KSTableName, opts.KSTableName, "The name of the table to read from", ""),
		&version.Flag{AppName: Name},
		sslFlag,
	)
	// Create and add the kvdb flag set
	kvdbSet := kvdbflag.NewSet(nil)
	flag.AddSet(kvdbSet)
	// Create and add the logd flag set
	logdSet := logdflag.NewLogSet(nil)
	flag.AddSet(logdSet)
	// Create and add the metricsdb flag set
	metricsdbSet := metricsdbflag.NewMetricsSet(nil)
	flag.AddSet(metricsdbSet)
	// Parse the flags
	flag.Parse()
	// Recover the SSL certificate
	cert, err := sslFlag.Certificate()
	if err != nil {
		return err
	}
	// Sanity check
	if opts.NumVertices <= 0 {
		return errors.New("the number of vertices must be a positive integer")
	}
	// Set the metrics
	if err := metricsdbSet.SetMetrics(context.Background(), Name, cert); err != nil {
		return err
	}
	// Set the kvdb values
	kvdbSet.SetDefault(Name, cert)
	// Set the loggers
	return logdSet.SetLogger(context.Background(), Name, cert)
}
