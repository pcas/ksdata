// kstest tests the performance of the kvdb Kreuzer--Skarke database under read and write load.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/kvdb"
	_ "bitbucket.org/pcas/keyvalue/kvdb/kvdbdriver"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/runtimemetrics"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"os"
	"time"
)

// inserter is the interface satisfied by the Insert method.
type inserter interface {
	Insert(context.Context, record.Iterator) error
}

// QueryAndInsertTimeout is the timeout for our select query and the creation of the results table
var QueryAndInsertTimeout = 1 * time.Hour

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

func logAndWait(lg log.Interface, format string, v ...interface{}) {
	lg.Printf(format, v...)
	// try to give the logger time to record that message
	time.Sleep(5 * time.Second)
	return
}

// connectToTable connects to the specified table in the specified database.
func connectToTable(ctx context.Context, dbName string, tableName string, lg log.Interface, m metrics.Interface) (*keyvalue.Table, error) {
	// Connect to the database
	dataSource := kvdb.DefaultConfig().URL(dbName)
	connCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	c, err := keyvalue.Open(connCtx, dataSource)
	cancel()
	if err != nil {
		return nil, err
	}
	cleanup.Add(c.Close)
	// Turn on logging and metrics
	c.SetLogger(lg)
	c.SetMetrics(m)
	// Connect to the  table
	connCtx, cancel = context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	return c.ConnectToTable(connCtx, tableName)
}

// runTest runs the load test.
func runTest(ctx context.Context, opts *Options, lg log.Interface, m metrics.Interface) error {
	// Connect to the output table
	tOut, err := connectToTable(ctx, opts.DatabaseName, opts.TableName, lg, m)
	if err != nil {
		return err
	}
	defer tOut.Close()
	// Connect to the KS table
	tKS, err := connectToTable(ctx, opts.KSDatabaseName, opts.KSTableName, lg, m)
	if err != nil {
		return err
	}
	defer tKS.Close()
	// Run the load test
	return buildVertexTable(ctx, opts.NumVertices, tKS, tOut, lg)
}

//////////////////////////////////////////////////////////////////////
// main function
//////////////////////////////////////////////////////////////////////

// runMain executes the main function, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse the options and make a note of the logger and metrics
	opts := setOptions()
	lg := log.Log()
	m := metrics.Metrics()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Record CPU usage etc
	defer func() {
		if e := runtimemetrics.Start(m).Stop(); err == nil {
			err = e
		}
	}()
	// Recover and log any panics
	defer func() {
		if e := recover(); e != nil {
			lg.Printf("Panic in main: %v", e)
			if err == nil {
				err = fmt.Errorf("panic in main: %v", e)
			}
		}
	}()
	// Run the tests
	err = runTest(ctx, opts, lg, m)
	return
}

func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
