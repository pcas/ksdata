// subtable contains the load test for the Kreuzer--Skarke mongodb database

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
)

// keysKS is the list of keys in a Kreuzer--Skarke kvdb record.  Treat this slice as a constant.
var keysKS = []string{
	"id",
	"number_of_vertices",
	"vertices",
	"number_of_boundary_points",
	"boundary_points",
	"number_of_edges",
	"number_of_2_faces",
	"number_of_facets",
	"hilbert_delta_1",
	"hilbert_delta_2",
	"volume",
	"degree",
	"is_simplicial",
	"is_smooth",
	"is_terminal",
	"h1",
	"h2",
	"h3",
	"h4",
	"h5",
	"h6",
	"h7",
	"h8",
	"h9",
	"h10",
}

func buildVertexTable(ctx context.Context, n int, tKS *keyvalue.Table, tOut *keyvalue.Table, lg log.Interface) error {
	// build a timeout for the query
	ctx, cancel := context.WithTimeout(ctx, QueryAndInsertTimeout)
	defer cancel()
	// build a query and a template
	selector := record.Record{"number_of_vertices": int64(n)}
	template := record.Record{}
	for _, k := range keysKS {
		// the template contains all the keys
		template[k] = ""
	}
	recs, err := tKS.Select(ctx, template, selector, nil)
	if err != nil {
		return fmt.Errorf("error in select: %w", err)
	}
	// insert the records
	return tOut.Insert(ctx, recs)
}
