// Convert provides functions for converting between mathematical objects and keyvalue database records.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package convert

import (
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/ksdata/point"
	"bitbucket.org/pcastools/convert"
	"bitbucket.org/pcastools/slice"
	"errors"
	"fmt"
	"strconv"
	"strings"
)

// maxhm is the largest m for which hm = h^0(-mK) should be calculated.
const maxhm = 10

// conversionFunc is a function used to convert object types.
type conversionFunc func(interface{}) (interface{}, error)

// conversionFuncs is the map of keys to conversion functions. Any keys not in this map will be skipped. Treat this map as a constant.
var conversionFuncs = map[string]conversionFunc{
	"id":                        toInt64,
	"number_of_vertices":        toInt64,
	"vertices":                  toPoints,
	"number_of_boundary_points": toInt64,
	"boundary_points":           toPoints,
	"number_of_edges":           toInt64,
	"number_of_2_faces":         toInt64,
	"number_of_facets":          toInt64,
	"hilbert_delta_vector":      toInt64Slice,
	"hilbert_delta_1":           toInt64,
	"hilbert_delta_2":           toInt64,
	"volume":                    toInt64,
	"degree":                    toInt64,
	"is_simplicial":             toBool,
	"is_smooth":                 toBool,
	"is_terminal":               toBool,
	// h1: 			 		     toInt64,
	// ...	   (added by the init function)
	// hmaxhm: 	     			 toInt64,
}

// validateFunc is a function used to validate the record. This function is free to modify the given record in any way it wishes. A non-nil return value will halt validation with a failure.
type validateFunc func(record.Record) error

// validateFuncs is a slice of validation functions. The functions will be applied in order. Treat this slice as a constant.
var validateFuncs = []validateFunc{
	validateID,
	validateVertices,
	validateNumberOfVertices,
	validateBoundaryPoints,
	validateNumberOfBoundaryPoints,
	validatefVector,
	validateDeltaVector,
	validatehm,
	validateVolume,
	validateDegree,
	validateSmoothImplications,
	validateIsTerminal,
}

// validKeys is the map of allowed keys that the record returned by ToRecord will have after successful conversion and validation. Treat this map as a constant.
var validKeys = map[string]bool{
	"id":                        true,
	"number_of_vertices":        true,
	"vertices":                  true,
	"number_of_boundary_points": true,
	"boundary_points":           true,
	"number_of_edges":           true,
	"number_of_2_faces":         true,
	"number_of_facets":          true,
	"hilbert_delta_1":           true,
	"hilbert_delta_2":           true,
	"volume":                    true,
	"degree":                    true,
	"is_simplicial":             true,
	"is_smooth":                 true,
	"is_terminal":               true,
	// h1: 						 true,
	// ...     (added by the init function)
	// hmaxhm:   				 true,
}

/*
Example SQL set-up
------------------
CREATE DATABASE ks;
USE ks;
CREATE TABLE reflexive4 (
	id INT PRIMARY KEY,
	number_of_vertices INT,
	vertices STRING,
	number_of_boundary_points INT,
	boundary_points STRING,
	number_of_edges INT,
	number_of_2_faces INT,
	number_of_facets INT,
	hilbert_delta_1 INT,
	hilbert_delta_2 INT,
	volume INT,
	degree INT,
	is_simplicial BOOL,
	is_smooth BOOL,
	is_terminal BOOL,
	h1 INT,
	h2 INT,
	h3 INT,
	h4 INT,
	h5 INT,
	h6 INT,
	h7 INT,
	h8 INT,
	h9 INT,
	h10 INT
);
CREATE INDEX ON reflexive4 (number_of_vertices);
CREATE INDEX ON reflexive4 (number_of_boundary_points);
CREATE INDEX ON reflexive4 (number_of_edges);
CREATE INDEX ON reflexive4 (number_of_2_faces);
CREATE INDEX ON reflexive4 (number_of_facets);
CREATE INDEX ON reflexive4 (volume);
CREATE INDEX ON reflexive4 (degree);
CREATE INDEX ON reflexive4 (hilbert_delta_1, hilbert_delta_2);
CREATE INDEX ON reflexive4 (h1, h2, h3, h4, h5);
*/

/* for PostgreSQL, this should be:

CREATE DATABASE ks;
\c ks;
CREATE TABLE reflexive4 (
	id INT PRIMARY KEY,
	number_of_vertices INT,
	vertices VARCHAR,
	number_of_boundary_points INT,
	boundary_points VARCHAR,
	number_of_edges INT,
	number_of_2_faces INT,
	number_of_facets INT,
	hilbert_delta_1 INT,
	hilbert_delta_2 INT,
	volume INT,
	degree INT,
	is_simplicial BOOL,
	is_smooth BOOL,
	is_terminal BOOL,
	h1 INT,
	h2 INT,
	h3 INT,
	h4 INT,
	h5 INT,
	h6 INT,
	h7 INT,
	h8 INT,
	h9 INT,
	h10 INT
);
CREATE INDEX ON reflexive4 (number_of_vertices);
CREATE INDEX ON reflexive4 (number_of_boundary_points);
CREATE INDEX ON reflexive4 (number_of_edges);
CREATE INDEX ON reflexive4 (number_of_2_faces);
CREATE INDEX ON reflexive4 (number_of_facets);
CREATE INDEX ON reflexive4 (volume);
CREATE INDEX ON reflexive4 (degree);
CREATE INDEX ON reflexive4 (hilbert_delta_1, hilbert_delta_2);
CREATE INDEX ON reflexive4 (h1, h2, h3, h4, h5);
*/

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initialised our memory.
func init() {
	// Add the hm to conversionFuncs
	for i := 1; i <= maxhm; i++ {
		conversionFuncs["h"+strconv.Itoa(i)] = toInt64
	}
	// Add the hm to validKeys
	for i := 1; i <= maxhm; i++ {
		validKeys["h"+strconv.Itoa(i)] = true
	}
}

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// toBool returns x as a boolean.
func toBool(x interface{}) (interface{}, error) {
	b, err := convert.ToBool(x)
	if err != nil {
		if s, ok := x.(string); ok && s == "" {
			b, err = false, nil
		}
	}
	return b, err
}

// recoverBool returns the value in r associated with the key k as a bool. This will panic if the key is set, but the value is not of the required type.
func recoverBool(r record.Record, k string) (bool, bool) {
	x, ok := r[k]
	if !ok {
		return false, false
	}
	return x.(bool), true
}

// toInt64 returns x as an int64.
func toInt64(x interface{}) (interface{}, error) {
	n, err := convert.ToInt64(x)
	if err != nil {
		if s, ok := x.(string); ok && s == "" {
			n, err = 0, nil
		}
	}
	return n, err
}

// recoverInt64 returns the value in r associated with the key k as an int64. This will panic if the key is set, but the value is not of the required type.
func recoverInt64(r record.Record, k string) (int64, bool) {
	x, ok := r[k]
	if !ok {
		return 0, false
	}
	return x.(int64), true
}

// toInt64Slice returns x as a []int64.
func toInt64Slice(x interface{}) (interface{}, error) {
	if S, ok := x.([]int64); ok {
		return S, nil
	} else if S, ok := x.([]string); ok {
		T := make([]int64, 0, len(S))
		for _, s := range S {
			n, err := strconv.ParseInt(strings.TrimSpace(s), 10, 64)
			if err != nil {
				return nil, err
			}
			T = append(T, n)
		}
		return T, nil
	}
	s, err := convert.ToString(x)
	if err != nil {
		return nil, err
	}
	return slice.Int64SliceFromString(s)
}

// recoverInt64Slice returns the value in r associated with the key k as a []int64. This will panic if the key is set, but the value is not of the required type.
func recoverInt64Slice(r record.Record, k string) ([]int64, bool) {
	x, ok := r[k]
	if !ok {
		return nil, false
	}
	return x.([]int64), true
}

// toPoints returns x as a point.Points.
func toPoints(x interface{}) (interface{}, error) {
	switch s := x.(type) {
	case string:
		return point.ParsePoints(s)
	case point.Points:
		return s, nil
	case point.Point:
		return point.Points{s}, nil
	default:
		return nil, fmt.Errorf("unable to convert type %T to a slice of 4-dimensional lattice points", x)
	}
}

// recoverPoints returns the value in r associated with the key k as a point.Points. This will panic if the key is set, but the value is not of the required type.
func recoverPoints(r record.Record, k string) (point.Points, bool) {
	x, ok := r[k]
	if !ok {
		return nil, false
	}
	return x.(point.Points), true
}

/////////////////////////////////////////////////////////////////////////
// Validation functions
/////////////////////////////////////////////////////////////////////////

// validateID validates the ID.
func validateID(r record.Record) error {
	if id, ok := recoverInt64(r, "id"); ok && (id < 1 || id > 473800776) {
		return fmt.Errorf("the ID (%d) must be in the range 1 to 473800776", id)
	}
	return nil
}

// validateVertices validates the vertices.
func validateVertices(r record.Record) error {
	// Extract the vertices
	S, ok := recoverPoints(r, "vertices")
	if !ok {
		return nil
	} else if len(S) == 0 {
		delete(r, "vertices")
		return nil
	}
	// There must be at least five vertices
	if len(S) < 5 {
		return fmt.Errorf("too few vertices (%d)", len(S))
	}
	// If the number of vertices is set, check that it agrees; otherwise set it
	if num, _ := recoverInt64(r, "number_of_vertices"); num == 0 {
		r["number_of_vertices"] = int64(len(S))
	} else if num != int64(len(S)) {
		return fmt.Errorf("number of vertices does not match (%d != %d)", num, len(S))
	}
	// Replace the vertices with their string representation
	r["vertices"] = S.String()
	return nil
}

// validateNumberOfVertices validates the number of vertices.
func validateNumberOfVertices(r record.Record) error {
	num, ok := recoverInt64(r, "number_of_vertices")
	if ok {
		if num == 0 {
			delete(r, "number_of_vertices")
		} else if num < 5 {
			return fmt.Errorf("invalid number of vertices (%d)", num)
		}
	}
	return nil
}

// validateBoundaryPoints validates the boundary points.
func validateBoundaryPoints(r record.Record) error {
	// Extract the boundary points
	S, ok := recoverPoints(r, "boundary_points")
	if !ok {
		return nil
	} else if len(S) == 0 {
		delete(r, "boundary_points")
		return nil
	}
	// The number of boundary points must be at least the number of vertices
	num, _ := recoverInt64(r, "number_of_vertices")
	if num == 0 {
		num = 5
	}
	if len(S) < int(num) {
		return fmt.Errorf("too few boundary points (%d)", len(S))
	}
	// If the number of boundary points is set, check that it agrees; otherwise
	// set it
	if num, _ := recoverInt64(r, "number_of_boundary_points"); num == 0 {
		r["number_of_boundary_points"] = int64(len(S))
	} else if num != int64(len(S)) {
		return fmt.Errorf("number of boundary points does not match (%d != %d)", num, len(S))
	}
	// Replace the boundary points with their string representation
	r["boundary_points"] = S.String()
	return nil
}

// validateNumberOfBoundaryPoints validates the number of boundary points.
func validateNumberOfBoundaryPoints(r record.Record) error {
	num, ok := recoverInt64(r, "number_of_boundary_points")
	if ok {
		if num == 0 {
			delete(r, "number_of_boundary_points")
		} else if num < 5 {
			return fmt.Errorf("invalid number of boundary points (%d)", num)
		}
	}
	return nil
}

// validatefVector validates the f-vector; that is, the number of vertices, the number of edges, number of 2-faces, and number of facets.
func validatefVector(r record.Record) error {
	// Check the f-vector and compute the Euler number as we do
	hasMissing := false
	euler := int64(0)
	sign := int64(1)
	for _, k := range []string{
		"number_of_vertices",
		"number_of_edges",
		"number_of_2_faces",
		"number_of_facets",
	} {
		n, _ := recoverInt64(r, k)
		if n == 0 {
			hasMissing = true
			delete(r, k)
		} else if n < 5 {
			return fmt.Errorf("illegal entry in f-vector (%s=%d)", k, n)
		}
		euler += sign * n
		sign *= -1
	}
	// If there are no missing entries, we check that the Euler number is 0
	if !hasMissing && euler != 0 {
		return fmt.Errorf("non-zero Euler number (%d)", euler)
	}
	return nil
}

// validateDeltaVector validates the Hilbert delta-vector.
func validateDeltaVector(r record.Record) error {
	// Recover the valids of delta_1 and delta_2
	d1, ok1 := recoverInt64(r, "hilbert_delta_1")
	d2, ok2 := recoverInt64(r, "hilbert_delta_2")
	// If the delta vector is set, recover it and use it to validate the delta_i
	if D, _ := recoverInt64Slice(r, "hilbert_delta_vector"); len(D) > 0 {
		// Sanity check
		if len(D) != 5 || D[0] != 1 || D[1] != D[3] || D[4] != 1 {
			return fmt.Errorf("invalid Hilbert delta-vector (%s)", slice.JoinInt64Slice(D, ","))
		} else if ok1 && d1 != D[1] {
			return fmt.Errorf("the Hilbert delta-vector (%s) does not agree with delta_1 (%d)", slice.JoinInt64Slice(D, ","), d1)
		} else if ok2 && d2 != D[2] {
			return fmt.Errorf("the Hilbert delta-vector (%s) does not agree with delta_2 (%d)", slice.JoinInt64Slice(D, ","), d2)
		}
		// Update the values of delta_1 and delta_2
		d1, d2 = D[1], D[2]
		ok1, ok2 = true, true
		r["hilbert_delta_1"], r["hilbert_delta_2"] = d1, d2
	}
	// If h1 and h2 are set, recover them and use them to validate the delta_i
	if h1, _ := recoverInt64(r, "h1"); h1 > 0 {
		if h2, _ := recoverInt64(r, "h2"); h2 >= h1 {
			// Compute the expected values of delta_1 and delta_2 from h1 and h2
			dd1 := h1 - 5
			dd2 := h2 - 5*h1 + 10
			// Sanity check
			if ok1 && d1 != dd1 {
				return fmt.Errorf("the Hilbert delta_1 (%d) does not equal h^0(-K)-5 (%d)", d1, dd1)
			} else if ok2 && d2 != dd2 {
				return fmt.Errorf("the Hilbert delta_2 (%d) does not equal h^0(-2K)-5*h^0(-K)+10 (%d)", d2, dd2)
			}
			// Update the values of delta_1 and delta_2
			d1, d2 = dd1, dd2
			ok1, ok2 = true, true
			r["hilbert_delta_1"], r["hilbert_delta_2"] = d1, d2
		}
	}
	// Sanity check on the degree
	if ok1 && ok2 && 2+2*d1+d2 < 5 {
		return fmt.Errorf("the Hilbert delta_1 (%d) and delta_2 (%d) give invalid volume (%d)", d1, d2, 2+2*d1+d2)
	}
	return nil
}

// validatehm validates the hm = h^0(-mK).
func validatehm(r record.Record) error {
	// Check that 1 <= h1 <= h2 <= ... is a weakly increasing sequence
	lasth := int64(1)
	for i := 1; i <= maxhm; i++ {
		k := "h" + strconv.Itoa(i)
		if h, _ := recoverInt64(r, k); h == 0 {
			delete(r, k)
		} else {
			if h < lasth {
				return fmt.Errorf("value of h%d (%d) is not weakly increasing", i, h)
			}
			lasth = h
		}
	}
	// Recover delta_1 and delta_2
	d1, ok := recoverInt64(r, "hilbert_delta_1")
	if !ok {
		return nil
	}
	d2, ok := recoverInt64(r, "hilbert_delta_2")
	if !ok {
		return nil
	}
	// Create the Hilbert polynomial
	L := func(t int64) int64 {
		t2 := t * t   // t^2
		t3 := t2 * t  // t^3
		t4 := t2 * t2 // t^4
		c := (2*d1+d2+2)*t4 + (4*d1+2*d2+4)*t3 + (10*d1-d2+46)*t2 + (8*d1-2*d2+44)*t + 24
		return c / 24
	}
	// Check (and fill in any missing entries of) the sequence of hm
	for i := 1; i <= maxhm; i++ {
		expectedh := L(int64(i))
		k := "h" + strconv.Itoa(i)
		if h, ok := recoverInt64(r, k); ok && h != expectedh {
			return fmt.Errorf("value of h%d (%d) does not agree with the value expected by the Hilbert polynomial (%d)", i, h, expectedh)
		}
		r[k] = expectedh
	}
	return nil
}

// validateVolume validates the volume.
func validateVolume(r record.Record) error {
	if vol, _ := recoverInt64(r, "volume"); vol == 0 {
		delete(r, "volume")
	} else if vol < 5 {
		return fmt.Errorf("volume too small (%d)", vol)
	}
	return nil
}

// validateDegree validates the degree.
func validateDegree(r record.Record) error {
	// Recover the degree
	deg, ok := recoverInt64(r, "degree")
	if ok && deg == 0 {
		delete(r, "degree")
		ok = false
	}
	// Does the degree agree with the Hilbert delta-vector?
	if delta1, ok := recoverInt64(r, "hilbert_delta_1"); ok {
		if delta2, ok := recoverInt64(r, "hilbert_delta_2"); ok {
			hilbDeg := 2 + 2*delta1 + delta2
			if !ok {
				ok = true
				deg = hilbDeg
				r["degree"] = deg
			} else if deg != hilbDeg {
				return fmt.Errorf("degree (%d) does not agree with the degree given by the Hilbert delta-vector (%d)", deg, hilbDeg)
			}
		}
	}
	// Is the degree large enough?
	if ok && deg < 5 {
		return fmt.Errorf("degree too small (%d)", deg)
	}
	return nil
}

// validateSmoothImplications checks if r satisfies smooth => (simplicial and terminal).
func validateSmoothImplications(r record.Record) error {
	// Is there anything to do?
	if isSmooth, ok := recoverBool(r, "is_smooth"); !ok || !isSmooth {
		return nil
	}
	// We'd better be marked as simplicial
	if isSimplicial, ok := recoverBool(r, "is_simplicial"); ok && !isSimplicial {
		return errors.New("marked as smooth but not simplicial")
	}
	r["is_simplicial"] = true
	// We'd better be marked as terminal
	if isTerminal, ok := recoverBool(r, "is_terminal"); ok && !isTerminal {
		return errors.New("marked as smooth but not terminal")
	}
	r["is_terminal"] = true
	return nil
}

// validateIsTerminal validates the value of is_terminal.
func validateIsTerminal(r record.Record) error {
	// Recover the number of vertices and number of boundary points (if known)
	num1, ok1 := recoverInt64(r, "number_of_vertices")
	num2, ok2 := recoverInt64(r, "number_of_boundary_points")
	// Recover whether we're marked as terminal
	isTerminal, ok3 := recoverBool(r, "is_terminal")
	// Does this make sense?
	if ok1 && ok2 {
		// Sanity check
		if ok3 && isTerminal != (num1 == num2) {
			return fmt.Errorf("terminal flag (%v) not compatible with the number of vertices (%d) and boundary points (%d)", isTerminal, num1, num2)
		}
		// Mark whether we're terminal
		r["is_terminal"] = num1 == num2
	} else if ok3 && (ok1 || ok2) {
		// Since we know we're terminal, we can set the missing number of points
		if ok1 {
			r["number_of_boundary_points"] = num1
		} else {
			r["number_of_vertices"] = num2
		}
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// FromKeyValueSlices attempts to convert the given parallel slices of keys and values into a record of Kreuzer--Skarke data. Any unknown keys will be skipped. The data will be validated and, where possible, any missing data computed.
func FromKeyValueSlices(keys []string, values []string) (record.Record, error) {
	// Sanity check
	if len(keys) != len(values) {
		return nil, fmt.Errorf("number of keys (%d) and number of values (%d) are not equal", len(keys), len(values))
	}
	// Make an initial pass over the data using the conversion functions
	r := make(record.Record, len(keys))
	for i, k := range keys {
		if f, ok := conversionFuncs[k]; ok {
			x, err := f(values[i])
			if err != nil {
				return nil, fmt.Errorf("error converting value for key \"%s\": %w", k, err)
			}
			r[k] = x
		}
	}
	// Apply the validation functions
	for _, f := range validateFuncs {
		if err := f(r); err != nil {
			return nil, fmt.Errorf("validation failed: %w", err)
		}
	}
	// Delete any invalid keys
	for k := range r {
		if !validKeys[k] {
			delete(r, k)
		}
	}
	// Finally, validate the types of the values in the record and return
	if _, err := r.IsValid(); err != nil {
		return nil, err
	}
	return r, nil
}
