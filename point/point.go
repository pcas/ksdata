// Point describes 4-dimension lattice points.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package point

import (
	"bitbucket.org/pcastools/stringsbuilder"
	"errors"
	"fmt"
	"math"
	"strconv"
	"strings"
	"unicode"
)

// Point describes a 4-dimensional lattice point.
type Point [4]int16

// Points is a slice of points.
type Points []Point

/////////////////////////////////////////////////////////////////////////
// Point functions
/////////////////////////////////////////////////////////////////////////

// Parse attempts to convert the given string to a point. The string is expected to be in the format
//	[ c1, c2, c3, c4 ]
// where the white-space is optional and flexible.
func Parse(s string) (Point, error) {
	var pt Point
	s = strings.TrimSpace(s)
	n := len(s)
	if n < 2 || s[0] != '[' || s[n-1] != ']' {
		return pt, errors.New("invalid brackets")
	}
	S := strings.Split(s[1:n-1], ",")
	if len(S) != 4 {
		return pt, fmt.Errorf("illegal number of coefficients: %d", len(S))
	}
	for i, x := range S {
		c, err := strconv.ParseInt(strings.TrimSpace(x), 10, 64)
		if err != nil {
			return pt, fmt.Errorf("error parsing coefficient %d: %w", i+1, err)
		} else if c < math.MinInt16 || c > math.MaxInt16 {
			return pt, fmt.Errorf("coefficient %d is out of range", i+1)
		}
		pt[i] = int16(c)
	}
	return pt, nil
}

// String returns a string description of the point. The string is formatted:
//	[c1,c2,c3,c4]
func (pt Point) String() string {
	b := stringsbuilder.New()
	b.WriteByte('[')
	for i := 0; i < 4; i++ {
		if i != 0 {
			b.WriteByte(',')
		}
		b.WriteString(strconv.Itoa(int(pt[i])))
	}
	b.WriteByte(']')
	s := b.String()
	stringsbuilder.Reuse(b)
	return s
}

/////////////////////////////////////////////////////////////////////////
// Points functions
/////////////////////////////////////////////////////////////////////////

// ParsePoints attempts to convert the given string to a slice of points. The string is expected to be in the format
//	[ [ c1, c2, c3, c4 ], ... ]
// where the white-space is optional and flexible.
func ParsePoints(s string) (Points, error) {
	s = strings.TrimSpace(s)
	n := len(s)
	if n < 2 || s[0] != '[' || s[n-1] != ']' {
		return nil, errors.New("invalid brackets")
	}
	s = strings.TrimSpace(s[1 : n-1])
	pts := make(Points, 0)
	for len(s) != 0 {
		idx := strings.Index(s, "]")
		if idx == -1 {
			return nil, errors.New("malformed string")
		}
		// Parse the point
		pt, err := Parse(s[:idx+1])
		if err != nil {
			return nil, fmt.Errorf("error parsing point %d: %w", len(pts)+1, err)
		}
		pts = append(pts, pt)
		// Move on
		if idx == len(s)-1 {
			s = ""
		} else {
			s = strings.TrimLeftFunc(s[idx+1:], unicode.IsSpace)
			if len(s) < 2 || s[0] != ',' {
				return nil, errors.New("malformed string")
			}
			s = s[1:]
		}
	}
	return pts, nil
}

// String returns a string description of the points. The string is formatted:
//	[[c1,c2,c3,c4],...]
func (pts Points) String() string {
	b := stringsbuilder.New()
	b.WriteByte('[')
	for i, pt := range pts {
		if i != 0 {
			b.WriteByte(',')
		}
		b.WriteByte('[')
		for j := 0; j < 4; j++ {
			if j != 0 {
				b.WriteByte(',')
			}
			b.WriteString(strconv.Itoa(int(pt[j])))
		}
		b.WriteByte(']')
	}
	b.WriteByte(']')
	s := b.String()
	stringsbuilder.Reuse(b)
	return s
}
